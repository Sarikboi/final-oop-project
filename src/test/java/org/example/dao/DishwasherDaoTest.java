package org.example.dao;

import org.example.entity.dishwasher;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class DishwasherDaoTest {

    private DishwasherDao dishwasherDao;

    @BeforeEach
    public void setUp() {
        File testDataFile = new File("test_data.csv");
        dishwasherDao = new DishwasherDao(testDataFile);
    }

    @Test
    public void testGetProducts() {
        try {
            List<dishwasher> products = dishwasherDao.getProducts();
            Assertions.assertNotNull(products);
            Assertions.assertFalse(products.isEmpty());
        } catch (IOException e) {
            Assertions.fail("An exception occurred while getting products: " + e.getMessage());
        }
    }
}