package org.example.dao;

import org.example.entity.microwave;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class MicrowaveDaoTest {

    private MicrowaveDao microwaveDao;

    @BeforeEach
    public void setUp() {
        File testDataFile = new File("test_data.csv");
        microwaveDao = new MicrowaveDao(testDataFile);
    }

    @Test
    public void testGetProducts() {
        try {
            List<microwave> products = microwaveDao.getProducts();
            Assertions.assertNotNull(products);
            Assertions.assertFalse(products.isEmpty());
        } catch (IOException e) {
            Assertions.fail("An exception occurred while getting products: " + e.getMessage());
        }
    }
}