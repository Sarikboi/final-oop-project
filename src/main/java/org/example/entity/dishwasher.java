package org.example.entity;

import java.util.Objects;

public class dishwasher extends entity {
    private int id;
    private  String name;
    private String madeIn;
    private double price;
    private int power;
    private int quantity;
    private double weight;

    public dishwasher(int i, String datum, String datum1, int i1, int i2, int i3, int parseInt) {
        super();
        this.id = i;
        this.name = datum;
        this.madeIn = datum1;
        this.price = i1;
        this.power = i2;
        this.quantity = i3;
        this.weight = parseInt;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public String getMadeIn() {
        return madeIn;
    }

    public void setMadeIn(String madeIn) {
        this.madeIn = madeIn;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public void setPrice(double price) {
        this.price = price;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    @Override
    public int getQuantity() {
        return quantity;
    }

    @Override
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        dishwasher that = (dishwasher) o;
        return id == that.id && Double.compare(that.price, price) == 0 && power == that.power && quantity == that.quantity && Double.compare(that.weight, weight) == 0 && Objects.equals(name, that.name) && Objects.equals(madeIn, that.madeIn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id, name, madeIn, price, power, quantity, weight);
    }

    @Override
    public String toString() {
        return "Dish Washer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", madeIn='" + madeIn + '\'' +
                ", price=" + price +
                ", power=" + power +
                ", quantity=" + quantity +
                ", weight=" + weight +
                '}';
    }
}