package org.example.constructor;

import java.io.IOException;
import java.util.Scanner;

public interface Controller {
void searchByAll(Scanner all) throws IOException;
void searchById(Scanner id) throws IOException;
void searchByName(Scanner name) throws IOException;
void searchByQuantity(Scanner quantity) throws IOException;
void searchByPrice(Scanner price) throws IOException;
}
