package org.example.constructor;

import org.example.entity.dishwasher;
import org.example.entity.fridge;
import org.example.entity.microwave;
import org.example.services.servicesDishWasher;
import org.example.services.servicesFridge;
import org.example.services.servicesMicrowave;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

public class controllerImpl implements Controller {

    private final servicesDishWasher dishwasherService;
    private final servicesFridge servicesFridge;
    private final servicesMicrowave servicesMicrowave;

    public controllerImpl(servicesDishWasher dishwasherService, servicesFridge servicesFridge, servicesMicrowave servicesMicrowave) {
        this.dishwasherService = dishwasherService;
        this.servicesFridge = servicesFridge;
        this.servicesMicrowave =  servicesMicrowave;
    }

    @Override
    public void searchByAll(Scanner all) throws IOException {
        List<dishwasher> products = (List<dishwasher>) dishwasherService.getAll();
        List<fridge> productsFridge = (List<fridge>) servicesFridge.getAll();
        List<microwave> productsMicrowave = (List<microwave>) servicesMicrowave.getAll();
        System.out.println(products);
        System.out.println(productsFridge);
        System.out.println(productsMicrowave);
    }

    @Override
    public void searchById(Scanner id) throws  IOException {
        System.out.print("Enter ID: ");
        int productId = id.nextInt();
        dishwasherService.getID(productId);
        servicesFridge.getID(productId);
        servicesMicrowave.getID(productId);
    }

    @Override
    public void searchByName(Scanner name) throws IOException {
        System.out.print("Enter name: ");
        String productName = name.nextLine();
        dishwasherService.getName(productName);
        servicesFridge.getName(productName);
        servicesMicrowave.getName(productName);
    }


    @Override
    public void searchByQuantity(Scanner quantity) throws IOException {
        System.out.print("Enter quantity: ");
        int productQuantity = quantity.nextInt();
        dishwasherService.getQuantity(productQuantity);
        servicesFridge.getQuantity(productQuantity);
        servicesMicrowave.getQuantity(productQuantity);
    }

    @Override
    public void searchByPrice(Scanner price) throws IOException {
        System.out.print("Enter price: ");
        double productPrice = price.nextDouble();
        dishwasherService.getPrice(productPrice);
        servicesFridge.getPrice(productPrice);
        servicesMicrowave.getPrice(productPrice);
    }
}