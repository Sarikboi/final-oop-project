package org.example.dao;
import org.example.entity.microwave;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class MicrowaveDao {

    private File file;

    public MicrowaveDao(File file) {
        this.file = file;
    }

    public List<microwave> getProducts() throws IOException {
        List<microwave> products = new ArrayList<>();
        try (
                FileReader reader = new FileReader(file);
                BufferedReader bufferedReader = new BufferedReader(reader);
        ) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] data = line.split(",");
                products.add(new microwave(
                        Integer.parseInt(data[0]),
                        data[1],
                        data[2],
                        Integer.parseInt(data[3]),
                        Integer.parseInt(data[4]),
                        Integer.parseInt(data[5]),
                        Integer.parseInt(data[6])
                ));
            }
        }
        return products;
    }

}
