package org.example.dao;

import org.example.entity.dishwasher;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class DishwasherDao {

    private File file;

    public DishwasherDao(File file) {
        this.file = file;
    }

    public List<dishwasher> getProducts() throws IOException {
        List<dishwasher> products = new ArrayList<>();
        try (
                FileReader reader = new FileReader(file);
                BufferedReader bufferedReader = new BufferedReader(reader);
        ) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] data = line.split(",");
                products.add(new dishwasher(
                        Integer.parseInt(data[0]),
                        data[1],
                        data[2],
                        Integer.parseInt(data[3]),
                        Integer.parseInt(data[4]),
                        Integer.parseInt(data[5]),
                        Integer.parseInt(data[6])
                ));
            }
        }
        return products;
    }

}