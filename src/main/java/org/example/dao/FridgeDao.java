package org.example.dao;
import org.example.entity.fridge;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FridgeDao {

    private File file;

    public FridgeDao(File file) {
        this.file = file;
    }

    public List<fridge> getProducts() throws IOException {
        List<fridge> products = new ArrayList<>();
        try (
                FileReader reader = new FileReader(file);
                BufferedReader bufferedReader = new BufferedReader(reader);
        ) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] data = line.split(",");
                products.add(new fridge(
                        Integer.parseInt(data[0]),
                        data[1],
                        data[2],
                        Integer.parseInt(data[3]),
                        Integer.parseInt(data[4]),
                        Integer.parseInt(data[5]),
                        Integer.parseInt(data[6])
                ));
            }
        }
        return products;
    }

}
