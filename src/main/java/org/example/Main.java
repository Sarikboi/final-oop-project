package org.example;

import org.example.constructor.controllerImpl;
import org.example.constructor.Controller;
import org.example.dao.DishwasherDao;
import org.example.dao.FridgeDao;
import org.example.dao.MicrowaveDao;
import org.example.services.servicesDishWasher;
import org.example.services.servicesFridge;
import org.example.services.servicesMicrowave;

import java.io.File;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        System.out.println("Sardorbek_Tukhtasinov@student.itpu.uz");

        DishwasherDao dishwasherDAO = new DishwasherDao(new File("src/main/resources/Dishwasher.csv"));
        FridgeDao daoFridge = new FridgeDao(new File("src/main/resources/Fridge.csv"));
        MicrowaveDao MicrowaveDao = new MicrowaveDao(new File("src/main/resources/Microwave.csv"));


        servicesDishWasher dishwasherService = new servicesDishWasher(dishwasherDAO);

        servicesFridge servicesFridge = new servicesFridge(daoFridge);

        servicesMicrowave servicesMicrowave = new servicesMicrowave(MicrowaveDao);

        Controller controller = new controllerImpl(dishwasherService, servicesFridge, servicesMicrowave);

        boolean exit = false;

        Scanner scanner = new Scanner(System.in);
        while (true){

        try {
            System.out.println("\n--Choose option--");
            System.out.println("1. Search by ID.");
            System.out.println("2. Search by Name.");
            System.out.println("3. Search by Quantity.");
            System.out.println("4. Search by Price.");
            System.out.println("5. Search by All.");
            System.out.println("6. Exit.");
            System.out.print("Enter your choice: ");
            int choice = scanner.nextInt();
            scanner.nextLine();

            switch (choice) {
                case 1:
                    controller.searchById(scanner);
                    break;
                case 2:
                    controller.searchByName(scanner);
                    break;
                case 3:
                    controller.searchByQuantity(scanner);
                    break;
                case 4:
                    controller.searchByPrice(scanner);
                    break;
                case 5:
                    controller.searchByAll(scanner);
                    break;
                case 6:
                    exit = true;
                    return;
                default:
                    System.out.println("Invalid choice!");
                    break;
            }
        } catch (IOException e) {
            System.out.println("An error occurred: " + e.getMessage());
        }  catch (InputMismatchException e) {
            System.err.println("\nInvalid input!");
            scanner.nextLine();
        }

     }
    }
}