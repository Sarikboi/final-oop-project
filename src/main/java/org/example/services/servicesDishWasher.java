package org.example.services;

import org.example.dao.DishwasherDao;
import org.example.entity.dishwasher;
import org.example.entity.entity;

import java.io.IOException;
import java.util.List;

public class servicesDishWasher implements Service {

    private final DishwasherDao dishwasherDAO;

    public servicesDishWasher(DishwasherDao dishwasherDAO) {
        this.dishwasherDAO = dishwasherDAO;
    }

    @Override
    public dishwasher getAll() throws IOException {
        List<dishwasher> products = dishwasherDAO.getProducts();
        for (dishwasher dishwasher : products){
            System.out.println(dishwasher);
        }
        return null;
    }

    @Override
    public dishwasher getID(int id) throws IOException {
        List<dishwasher> products = dishwasherDAO.getProducts();
        for (dishwasher product : products) {
            if (product.getId() == id) {
                System.out.println(product);
            }
        }
        return null;
    }

    @Override
    public entity getName(String name) throws IOException {
        List<dishwasher> products = dishwasherDAO.getProducts();
        for (dishwasher product : products) {
            if (product.getName().equalsIgnoreCase(name)) {
                System.out.println(product);
            }
        }
        return null;
    }

    @Override
    public entity getQuantity(int quantity) throws IOException {
        List<dishwasher> products = dishwasherDAO.getProducts();
        for (dishwasher product : products) {
            if (product.getQuantity() == quantity) {
                System.out.println(product);
            }
        }
        return null;
    }

    @Override
    public entity getPrice(double price) throws IOException {
        List<dishwasher> products = dishwasherDAO.getProducts();
        for (dishwasher product : products) {
            if (product.getPrice() == price) {
                System.out.println(product);
            }
        }
        return null;
    }
}