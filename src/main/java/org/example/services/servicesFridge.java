package org.example.services;

import org.example.dao.FridgeDao;
import org.example.entity.fridge;
import org.example.entity.entity;

import java.io.IOException;
import java.util.List;

public class servicesFridge implements Service {

    private final FridgeDao fridgeDAO;

    public servicesFridge(FridgeDao fridgeDAO) {
        this.fridgeDAO = fridgeDAO;
    }

    @Override
    public fridge getAll() throws IOException {
        List<fridge> products = fridgeDAO.getProducts();
        for (fridge fridge : products){
            System.out.println(fridge);
        }
        return null;
    }


    @Override
    public fridge getID(int id) throws IOException {
        List<fridge> products = fridgeDAO.getProducts();
        for (fridge product : products) {
            if (product.getId() == id) {
                System.out.println(product);
            }
        }
        return null;
    }

    @Override
    public entity getName(String name) throws IOException {
        List<fridge> products = fridgeDAO.getProducts();
        for (fridge product : products) {
            if (product.getName().equalsIgnoreCase(name)) {
                System.out.println(product);
            }
        }
        return null;
    }

    @Override
    public entity getQuantity(int quantity) throws IOException {
        List<fridge> products = fridgeDAO.getProducts();
        for (fridge product : products) {
            if (product.getQuantity() == quantity) {
                System.out.println(product);
            }
        }
        return null;
    }

    @Override
    public entity getPrice(double price) throws IOException {
        List<fridge> products = fridgeDAO.getProducts();
        for (fridge product : products) {
            if (product.getPrice() == price) {
                System.out.println(product);
            }
        }
        return null;
    }
}