package org.example.services;

import org.example.dao.MicrowaveDao;
import org.example.entity.microwave;
import org.example.entity.entity;

import java.io.IOException;
import java.util.List;

public class servicesMicrowave implements Service {

    private final MicrowaveDao microwaveDAO;

    public servicesMicrowave(MicrowaveDao microwaveDAO) {
        this.microwaveDAO = microwaveDAO;
    }

    @Override
    public microwave getAll() throws IOException {
        List<microwave> products = microwaveDAO.getProducts();
        for (microwave microwave : products){
            System.out.println(microwave);
        }
        return null;
    }

    @Override
    public microwave getID(int id) throws IOException {
        List<microwave> products = microwaveDAO.getProducts();
        for (microwave product : products) {
            if (product.getId() == id) {
                System.out.println(product);
            }
        }
        return null;
    }

    @Override
    public entity getName(String name) throws IOException {
        List<microwave> products = microwaveDAO.getProducts();
        for (microwave product : products) {
            if (product.getName().equalsIgnoreCase(name)) {
                System.out.println(product);
            }
        }
        return null;
    }

    @Override
    public entity getQuantity(int quantity) throws IOException {
        List<microwave> products = microwaveDAO.getProducts();
        for (microwave product : products) {
            if (product.getQuantity() == quantity) {
                System.out.println(product);
            }
        }
        return null;
    }

    @Override
    public entity getPrice(double price) throws IOException {
        List<microwave> products = microwaveDAO.getProducts();
        for (microwave product : products) {
            if (product.getPrice() == price) {
                System.out.println(product);
            }
        }
        return null;
    }
}