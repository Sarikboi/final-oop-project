package org.example.services;

import org.example.entity.entity;

import java.io.IOException;

public interface Service {
    entity getAll() throws IOException;
    entity getID(int id) throws IOException;
    entity getName(String name) throws IOException;
    entity getQuantity(int quantity) throws IOException;
    entity getPrice(double price) throws IOException;
}
